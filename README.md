# BMI Calculator

A BMI Calculator with BMI Category, Health Risk,etc and counts overweight people.

### Steps to Run:

1. Clone Repository.
2. Install dependencies `npm install`
3. Run app `npm start`
4. Run Test `npm run test`

### API endpoints

1.  `/api/getbmiresult`

Api for small Input. Directly converting to Json.

Method: POST

Body [{
"Gender":"Male",
"HeightCm":171,
"WeightKg":96
}]

2. `/api/getbmiresultlarge`

Using Nodejs Stream to read data and send response without storing whole data at a single time in memory.

Method: POST

Body [{
"Gender":"Male",
"HeightCm":171,
"WeightKg":96
}]

### Automation

Using Github Action

[![Node.js CI](https://github.com/krishrahul98/code_12-07-2021_rahul_krishna/actions/workflows/node.js.yml/badge.svg)](https://github.com/krishrahul98/code_12-07-2021_rahul_krishna/actions/workflows/node.js.yml)
