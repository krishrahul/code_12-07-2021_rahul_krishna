const fs = require("fs");
const StreamArray = require("stream-json/streamers/StreamArray");

const bmiCategoryConst = require("../utils/constants").bmiCategoryConst;
const HttpError = require("../models/http-error");

const calculateBMILarge = async (req, res, next) => {
  let overWeightCounter = 0;

  //Create JSONStream parser
  const jsonStream = StreamArray.withParser();

  //Start Writing Response
  try {
    res.writeHead(200, { "Content-Type": "application/json" });
    res.write(`{"BMITable":[`);
    jsonStream.on("data", ({ key, value }) => {
      validateData(value);
      let bmiOut = determineCategory(value);
      overWeightCounter += bmiOut.BMICategory === "Over weight" ? 1 : 0;
      res.write(JSON.stringify(bmiOut) + "\n,");
    });
    jsonStream.on("end", () => {
      res.write(`],"OverWeightCount":${overWeightCounter}}`);
      res.end();
    });
  } catch (err) {
    return next(err);
  }

  req.pipe(jsonStream.input);
};

const calculateBMI = async (req, res, next) => {
  const bmiInput = req.body;
  try {
    let counter = 0;
    let bmiOutput = bmiInput.map((bmiElement) => {
      validateData(bmiElement);
      let bmiOut = determineCategory(bmiElement);
      counter += bmiOut.BMICategory === "Over weight" ? 1 : 0;
      return bmiOut;
    });
    res.json({ BMITable: bmiOutput, OverWeightCount: counter });
  } catch (error) {
    return next(error);
  }
};

const determineCategory = (data) => {
  let heightM = data.HeightCm / 100;

  //Calculate bmi and round off to 1 decimal point
  let bmi = Number((data.WeightKg / (heightM * heightM)).toFixed(1));
  let category;
  if (bmi < 18.5) {
    category = bmiCategoryConst.underWeight;
  } else if (bmi >= 18.5 && bmi < 25) {
    category = bmiCategoryConst.normalWeight;
  } else if (bmi >= 25 && bmi < 30) {
    category = bmiCategoryConst.overWeight;
  } else if (bmi >= 30 && bmi < 35) {
    category = bmiCategoryConst.moderatelyObese;
  } else if (bmi >= 35 && bmi < 40) {
    category = bmiCategoryConst.severelyObese;
  } else {
    category = bmiCategoryConst.verySeverelyObese;
  }
  let outputData = { ...data, Bmi: bmi, ...category };
  return outputData;
};

const validateData = (data) => {
  console.log(data);
  if (!data || !data.HeightCm || !data.WeightKg) {
    throw new HttpError("Empty data", 400);
  }
  let weight = Number(data.WeightKg);
  let height = Number(data.HeightCm);
  if (isNaN(weight) || isNaN(height) || weight <= 0 || height <= 0) {
    throw new HttpError("Invalid Input", 400);
  }
};

module.exports.calculateBMI = calculateBMI;
module.exports.calculateBMILarge = calculateBMILarge;
