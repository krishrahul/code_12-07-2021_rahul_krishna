const express = require("express");
const bodyParser = require("body-parser");
const bmiRouter = require("./routes/bmi-routes");
const HttpError = require("./models/http-error");

const app = express();
const PORT = process.env.PORT || 5000;

//app.use(express.json({ limit: "100mb" }));

app.use("/api", bmiRouter);

app.use((req, res, next) => {
  const error = new HttpError("Could not find this route.", 404);
  throw error;
});

app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }
  res.status(error.code || 500);
  res.json({ message: error.message || "An unknown error occurred!" });
});

const init = async () => {
  app.listen(PORT, () => console.log(`Server is running at Port ${PORT}...`));
};

module.exports.init = init;
module.exports.app = app;
