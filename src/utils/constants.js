const bmiCategoryConst = {
  underWeight: { BMICategory: "Under weight", HealthRisk: "Malnutrition risk" },
  normalWeight: { BMICategory: "Normal weight", HealthRisk: "Low risk" },
  overWeight: { BMICategory: "Over weight", HealthRisk: "Enhanced risk" },
  moderatelyObese: {
    BMICategory: "Moderately obese",
    HealthRisk: "Medium risk",
  },
  severelyObese: { BMICategory: "Severly obese", HealthRisk: "High risk" },
  verySeverelyObese: {
    BMICategory: "Very Severly obese",
    HealthRisk: "Very High risk",
  },
};

module.exports.bmiCategoryConst = bmiCategoryConst;
