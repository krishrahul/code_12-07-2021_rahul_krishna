const express = require("express");

const bmiController = require("../controller/bim-controller");

const router = express.Router();

router.post(
  "/getbmiresult",
  express.json({ limit: "100mb" }),
  bmiController.calculateBMI
);
router.post("/getbmiresultlarge", bmiController.calculateBMILarge);

module.exports = router;
