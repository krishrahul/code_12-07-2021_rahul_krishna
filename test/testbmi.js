const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../src/server");
const should = chai.should();

chai.use(chaiHttp);

server.init();
const app = server.app;

const inputData = [
  {
    Gender: "Male",
    HeightCm: 171,
    WeightKg: 96,
  },
  {
    Gender: "Male",
    HeightCm: 161,
    WeightKg: 85,
  },
];

const invalidInput = [
  {
    Gender: "Male",
    HeightCm: -171,
    WeightKg: 96,
  },
  {
    Gender: "Male",
    HeightCm: 161,
    WeightKg: 85,
  },
];

const outputData = [
  {
    Gender: "Male",
    HeightCm: 171,
    WeightKg: 96,
    Bmi: 32.8,
    BMICategory: "Moderately obese",
    HealthRisk: "Medium risk",
  },
  {
    Gender: "Male",
    HeightCm: 161,
    WeightKg: 85,
    Bmi: 32.8,
    BMICategory: "Moderately obese",
    HealthRisk: "Medium risk",
  },
];

describe("Calulate BMI", () => {
  it("should calculate BMI", (done) => {
    chai
      .request(app)
      .post("/api/getbmiresult")
      .send(inputData)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("BMITable");
        res.body.BMITable.should.eql(outputData);
        done();
      });
  });
  it("should throw Invalid input error", (done) => {
    chai
      .request(app)
      .post("/api/getbmiresult")
      .send(invalidInput)
      .end((err, res) => {
        res.should.have.status(400);
        res.body.should.be.a("object");
        res.body.should.have.property("message");
        res.body.message.should.equal("Invalid Input");
        done();
      });
  });
});
